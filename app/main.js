var app = angular.module("TareaAngularRutas", ["ngRoute"]);
app.config(function($routeProvider){
    $routeProvider
    .when("/employed", {templateUrl: "./app/view/employed.html", controller:"ctrlEmployed"})
    .when("/dice", {templateUrl: "./app/view/dice.html", controller:"ctrlDice"})
    .when("/perquisite", {templateUrl: "./app/view/perquisite.html", controller:"ctrlPerquisite"})
    .otherwise({redirectTo:"/employed"});
});
